import 'package:flutter_ps5_products/model/controller.dart';

class ItemSelectedState {
  final List<Item> selectedItem;
  final int selectedIndex;

  ItemSelectedState(this.selectedItem, this.selectedIndex);
}
